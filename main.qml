import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Window 2.2
import QtLocation 5.9
import QtPositioning 5.8

ApplicationWindow {
    id: gpsWindow
    visible: true
    width: 400
    height: 400

    title: qsTr("GPS Viewer")
    flags: Qt.FramelessWindowHint

    property string gpsAddress: ""
    property var currentCoordinate: QtPositioning.coordinate(32.968531, -96.715585)

    Plugin {
        id: mapPlugin
        name: "osm" // "mapboxgl", "esri", ...

        // specify plugin parameters if necessary
        PluginParameter {
             name: "osm.useragent";
             value: "Qt GPS Viewer"
         }

         /*name: "here"
         PluginParameter { name: "here.app_id"; value: "EjVmILs6PMihwDOnn4Re" }
         PluginParameter { name: "here.token"; value: "jy8UZVuixRd0aF_TjF42T4FMSdjOjTIYpP6X9ck0YVc" }
         PluginParameter { name: "here.proxy"; value:"system"}
         */
    }

    /*function addMarker(latitude, longitude)
        {
            var Component = Qt.createComponent("qrc:/Marker.qml")
            var item = Component.createObject(gpsWindow, {
                                                  coordinate: QtPositioning.coordinate(latitude, longitude)
                                              })
            map.addMapItem(item)
        }*/

    function addMarker(coordinateInfo)
        {
            var Component = Qt.createComponent("qrc:/Marker.qml")
            var item = Component.createObject(gpsWindow, {coordinate: coordinateInfo})
            map.addMapItem(item)
        }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        center: currentCoordinate
        zoomLevel: 14
        //activeMapType: supportedMapTypes[supportedMapTypes.length - 3]

        //! [mapnavigation]
            // Enable pan, flick, and pinch gestures to zoom in and out
            gesture.acceptedGestures: MapGestureArea.PanGesture | MapGestureArea.FlickGesture | MapGestureArea.PinchGesture | MapGestureArea.RotationGesture | MapGestureArea.TiltGesture
            gesture.flickDeceleration: 3000
            gesture.enabled: true
        //! [mapnavigation]
            focus: true


            Keys.onPressed: {
                if (event.key === Qt.Key_Plus) {
                    if(map.zoomLevel < 19)
                        map.zoomLevel++;
                } else if (event.key === Qt.Key_Minus) {
                    map.zoomLevel--;
                }
            }

            onZoomLevelChanged:{
               map.center = currentCoordinate
            }

        function geocodeMessage()
        {
            var street, district, city, county, state, countryCode, country, postalCode, latitude, longitude, text
            latitude = Math.round(geocodeModel.get(0).coordinate.latitude * 10000) / 10000
            longitude =Math.round(geocodeModel.get(0).coordinate.longitude * 10000) / 10000
            street = geocodeModel.get(0).address.street
            district = geocodeModel.get(0).address.district
            city = geocodeModel.get(0).address.city
            county = geocodeModel.get(0).address.county
            state = geocodeModel.get(0).address.state
            countryCode = geocodeModel.get(0).address.countryCode
            country = geocodeModel.get(0).address.country
            postalCode = geocodeModel.get(0).address.postalCode

            text = "<b>Latitude:</b> " + latitude + "<br/>"
            text +="<b>Longitude:</b> " + longitude + "<br/>" + "<br/>"
            if (street) text +="<b>Street: </b>"+ street + " <br/>"
            if (district) text +="<b>District: </b>"+ district +" <br/>"
            if (city) text +="<b>City: </b>"+ city + " <br/>"
            if (county) text +="<b>County: </b>"+ county + " <br/>"
            if (state) text +="<b>State: </b>"+ state + " <br/>"
            if (countryCode) text +="<b>Country code: </b>"+ countryCode + " <br/>"
            if (country) text +="<b>Country: </b>"+ country + " <br/>"
            if (postalCode) text +="<b>PostalCode: </b>"+ postalCode + " <br/>"

            for (var i = 0; i<map.supportedMapTypes.length; i++) {
                text += " " + map.supportedMapTypes[i].name + " "
            }            return text
        }

        function geocode(fromAddress)
        {
            //! [geocode1]
            // send the geocode request
            geocodeModel.query = fromAddress
            geocodeModel.update()
            //! [geocode1]
        }

        GeocodeModel {
            id: geocodeModel
            plugin: map.plugin
            onStatusChanged: {

                if (status == GeocodeModel.Ready) {
                    if (count == 0) {
                        console.debug("Geocode Ready: Unsuccessful geocode")
                    } else if (count > 1) {
                        console.debug("Ambiguous geocode results found for the given address, please specify location")
                    } else {
                        console.debug("Location: " + map.geocodeMessage())
                    }
                } else if (status == GeocodeModel.Error) {
                    console.debug("Geocode Error: Unsuccessful geocode")
                }
            }
            onLocationsChanged:
            {
                if (count == 1) {
                    //map.center.latitude = get(0).coordinate.latitude
                    //map.center.longitude = get(0).coordinate.longitude
                    gpsAddress = get(0).address.text
                }
            }
        }

        Component {
            id: pointDelegate

            MapCircle {
                id: point
                radius: 30
                color: "transparent"//"#46a2da"
                border.color: "transparent" //"#190a33"
                //border.width: 2
                //smooth: true
                //opacity: 0.5
                center: locationData.coordinate                         
            }
        }

        MapItemView {
            model: geocodeModel
            delegate: pointDelegate
        }

        Component.onCompleted: {
            addMarker(currentCoordinate);

            var coordinate = currentCoordinate;
            if (coordinate.isValid) {
                map.geocode(coordinate)
            }
        }
    }

    Timer  {
            id: elapsedTimer
            interval: 10000;
            running: true;
            repeat: false;
            onTriggered: Qt.quit()
        }

   Rectangle {
       id: addressArea
       color: "lightblue"
       width: parent.width
       height: 50
       anchors.bottom: parent.bottom
       Text {
           id: gpsText
           //color: "white"
           anchors.fill: parent
           text: gpsAddress
           font.pixelSize: parent.height * 0.3
           wrapMode: Text.Wrap
           horizontalAlignment: Text.AlignHCenter
           verticalAlignment: Text.AlignVCenter
       }
   }

   ZoomButton {
       id: zoomMinus
       anchors {
           bottom: addressArea.top
           bottomMargin: 8
           left: parent.left
           leftMargin: 8
       }

       text: "-"
       onClicked: {
           elapsedTimer.restart();
            map.zoomLevel--;
       }
   }

   ZoomButton {
       id: zoomPlus
       anchors {
           bottom: zoomMinus.top
           bottomMargin: 8
           left: parent.left
           leftMargin: 8
       }
       text: "+"
       onClicked: {
           elapsedTimer.restart();
           if(map.zoomLevel < 19)
                map.zoomLevel++;
       }
   }

    Component.onCompleted: {
        if (centerX && centerX) {
            setX(centerX - width/2);
            setY(centerY - height/2);
        }
    }

}
