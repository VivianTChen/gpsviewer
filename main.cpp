#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtDebug>



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);


    QQmlApplicationEngine engine;

    //set parent window centerX, centerY
    if (argc > 2 ) {
        int centerX = atoi(argv[1]);
        int centerY = atoi(argv[2]);
        qDebug() << "pass parameters: " << centerX << " and " << centerY;
        engine.rootContext()->setContextProperty("centerX", centerX);
        engine.rootContext()->setContextProperty("centerY", centerY);
      }
    else {
        engine.rootContext()->setContextProperty("centerX", nullptr);
        engine.rootContext()->setContextProperty("centerY", nullptr);
    }

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
