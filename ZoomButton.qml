import QtQuick 2.0

Rectangle {
    id: root

// public
    property string text: 'text'

    signal clicked();

// private
    width: 50;
    height: 50
    border.color: "grey"
    border.width: 1
    radius:       root.height / 2
    opacity:      enabled  &&  !mouseArea.pressed? 1: 0.3

    Text {
        anchors.centerIn: parent
        text: root.text
        font.pixelSize: parent.height * 0.6
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:  root.clicked()
    }
}
